from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from functions.close_popup_function import close_popup_function_1
from functions.login_function import login_function_1
import unittest


class Check_Login_Page(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://staging.mygeographic.com.au/customer/account/login/")
        self.driver.maximize_window()
        self.action = ActionChains(self.driver)


    def test_01_login_success(self):
        close_popup_function_1(self)
        login_function_1(self, 'autotest1@gmail.com', 'att120201@')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Account Information')]")
        assert self.result


    def test_02_login_fail_blank_field(self):
        close_popup_function_1(self)
        login_function_1(self, '', '')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'This is a required field.')]")
        assert self.result


    def test_03_login_fail_wrong_format_mail(self):
        close_popup_function_1(self)
        login_function_1(self, 'autotest1', 'att120201@')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Please enter a valid email address')]")
        assert self.result


    def test_04_login_fail_wrong_mail_pwd(self):
        close_popup_function_1(self)
        login_function_1(self, 'email_not_existed@gmail.com', 'pwdisincorrect')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'You did not sign in correctly')]")
        assert self.result


    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)