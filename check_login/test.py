from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from mygeographic.functions.close_popup_function import close_popup_function_1
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time

class Test(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://staging.mygeographic.com.au/")
        self.driver.maximize_window()
        self.action = ActionChains(self.driver)
        time.sleep(2)
        close_popup_function_1(self)


    def test_search(self):
        self.action = ActionChains(self.driver)
        #enter the key word to search
        self.search_key = 'book'
        #locate search box and enter search key
        self.driver.find_element_by_xpath("//input[@id='search']").send_keys(self.search_key)
        #locate and click on Search button
        self.search_btn = self.driver.find_element_by_xpath("//div[@class='field search']//following-sibling::div//button")
        self.search_btn.click()
        #verify if the search page is displayed
        self.result = "Search results for: " + "'" + self.search_key + "'"
        self.assertIn(self.result, self.driver.title)

        close_popup_function_1(self)
        time.sleep(5)
        #locate and click on the add to cart button for first available item
        #self.addtocart_btn = self.driver.find_element_by_xpath("//form[contains(@action,'8274')]//button")
        #self.addtocart_btn.click()
        #self.driver.execute_script("window.scrollBy(0, -1400);")
        self.add_to_basket = self.driver.find_elements_by_xpath("//form[@data-role='tocart-form']//button")
        #self.list_add_to_basket = list(self.add_to_basket)
        print(self.add_to_basket)
    

        time.sleep(5)
        #locate and click on Cart button
        self.showcart_btn = self.driver.find_element_by_xpath("//a[@class='action showcart']")
        self.showcart_btn.click()


        time.sleep(5)
        #verify if added item is displayed in My Cart page
        self.verifycart = self.driver.find_element_by_xpath("//*[contains(@href,'8274')]")
        assert self.verifycart


    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)

