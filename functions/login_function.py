def login_function_1(self, emails, password):
    self.email_form = self.driver.find_element_by_xpath("//div[@class='right-col']//input[@id='email']")
    self.email_form.clear()
    self.email_form.send_keys(emails)

    self.pwd_form = self.driver.find_element_by_xpath("//div[@class='right-col']//input[@id='pass']")
    self.pwd_form.clear()
    self.pwd_form.send_keys(password)

    self.login_btn = self.driver.find_element_by_xpath("//div[@class='field actions']//button[@id='send2']")
    self.login_btn.click()