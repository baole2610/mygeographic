def register_function_1(self, firstname, lastname, emails, password, cfpassword):
    self.first_name = self.driver.find_element_by_xpath("//div[contains(@class,'right-col')]//input[@id='firstname']")
    self.first_name.clear()
    self.first_name.send_keys(firstname)

    self.last_name = self.driver.find_element_by_xpath("//div[contains(@class,'right-col')]//input[@id='lastname']")
    self.last_name.clear()
    self.last_name.send_keys(lastname)

    self.email_form = self.driver.find_element_by_xpath("//input[@id='email_address']")
    self.email_form.clear()
    self.email_form.send_keys(emails)

    self.pwd_form = self.driver.find_element_by_xpath("//input[@id='password']")
    self.pwd_form.clear()
    self.pwd_form.send_keys(password)

    self.cf_pwd_form = self.driver.find_element_by_xpath("//input[@id='password-confirmation']")
    self.cf_pwd_form.clear()
    self.cf_pwd_form.send_keys(cfpassword)

    self.register_btn = self.driver.find_element_by_xpath("//button[@class='action submit primary']")
    self.register_btn.click()
