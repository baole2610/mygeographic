from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from functions.close_popup_function import close_popup_function_1
from functions.register_function import register_function_1
import unittest


class Check_Register_Page(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://staging.mygeographic.com.au/customer/account/create/")
        self.action = ActionChains(self.driver)


    def test_register_success(self):
        close_popup_function_1(self)
        register_function_1(self, 'Bao', 'Le', 'autotest8@gmail.com', 'att120208@', 'att120208@')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Thank you for registering')]")
        assert self.result


    def test_register_fail_existed_mail(self):
        close_popup_function_1(self)
        register_function_1(self, 'Bao', 'Le', 'autotest5@gmail.com', 'att120205@', 'att120205@')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'There is already an account with this email address')]")
        assert self.result


    def test_register_fail_blank_field(self):
        close_popup_function_1(self)
        register_function_1(self, '', '', '', '', '')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'This is a required field.')]")
        assert self.result


    def test_register_fail_wrong_format_mail(self):
        close_popup_function_1(self)
        register_function_1(self, 'Bao', 'Le', 'autotest5', 'att120205@', 'att120205@')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Please enter a valid email address')]")
        assert self.result


    def test_register_fail_wrong_format_pwd(self):
        close_popup_function_1(self)
        register_function_1(self, 'Bao', 'Le', 'autotest5@gmail.com', 'att120205', 'att120205')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Minimum of different classes of characters in password is 3')]")
        assert self.result


    def test_register_fail_wrong_cf_pwd(self):
        close_popup_function_1(self)
        register_function_1(self, 'Bao', 'Le', 'autotest5@gmail.com', 'att120205@', 'att120205')
        self.result = self.driver.find_element_by_xpath("//*[contains(text(),'Please enter the same value again.')]")
        assert self.result


    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)